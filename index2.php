<?php 

    include_once('db_access.php');

    $datas = execute_sql('SELECT * FROM `dates`;');

    $last_id = execute_sql('SELECT MAX(`id`) AS last_id FROM `dates`;');
    $last_id = $last_id[0]['last_id'];

?>


<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
    <script>
        $(function(){
            $('.datepicker').datepicker();
            var next_id = <?php echo ($last_id + 1); ?>;
            $('.btnAdd').on('click', function() {
                var tr = $('#copy_tr').clone(true);
                tr.attr('id','');
                tr.find('.date').attr('id','date_' + next_id);
                tr.find('.save').attr('data-id',next_id);
                tr.find('.date').datepicker();
                tr.appendTo('#mytable');

                next_id++;
            });
            $('.save').on('click', function() {
                var id = $(this).data('id');
                $('#form1_id').val(id);
                var date = $('#date_' + id).val();
                $('#form1_date').val(date);
            });

            $('.datepicker,.date').on('change', function() {
                var id = $(this).attr('id');
                id = id.replace(/date_/g, "");
                $('#form1_id').val(id);
                var date = $(this).val();
                $('#form1_date').val(date);
            });

            $('.datepicker,.date').on('change', function(){
                var id = $('#form1_id').val();
                var date = $('#form1_date').val();
                $.ajax({
                    url: "http://localhost/03/ajax.php",
                    type: "POST",
                    data: {id: id, date: date},
                    dataType: "json"
                }).done(function(data) {
                    alert("[" + data.status + "]" + data.id + ":" + data.date);
                }).fail(function(data){
                });
            });
        });
    </script>
    <title></title>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/cupertino/jquery-ui.css" >
</head>

<body>
    <table id="mytable">
    <?php foreach ($datas as $data): ?>
        <tr>
            <td><input type="text" class="datepicker" id="date_<?php echo $data['id'] ?>" value="<?php echo $data['date'] ?>"></td>
            <td><input type="button" class="save" data-id="<?php echo $data['id'] ?>" value="保存"></td>
        </tr>
    <?php endforeach; ?>    
    </table>
    <table style="display:none;">
        <tr id="copy_tr">
            <td><input type="text" class="date" id=""></td>
            <td><input type="button" class="save" data-id="" value="保存"></td>
        </tr>
    </table>
    <hr>
    <input type="button" class="btnAdd" value="行追加"><br>
    <form id="form1">
        <input type="text" id="form1_id" name="id" value=""></input>
        <input type="text" id="form1_date" name="date" value=""></input>
    </form>
    <input type="button" id="ajax" value="Ajax通信">
</body>

</html>